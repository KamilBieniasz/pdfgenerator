import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Document;

import java.io.FileOutputStream;


public class Main {
    private static String FILE = "E:/Programowanie/Java/resume/pdfGenerator.pdf";
    private static Font font = new Font(Font.FontFamily.TIMES_ROMAN,20);
    public static void main(String[] args){
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            document.add(new Chunk(""));
            addMetaData(document);
            addTitle(document);
            createTable(document);
            document.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    private static void addMetaData(Document document){
        document.addTitle("Resume");
    }
    private static void addTitle(Document document) throws DocumentException{
            Paragraph title = new Paragraph();
            title.add(new Paragraph("RESUME", font));
            title.setAlignment(Element.ALIGN_CENTER);
            addEmptyLine(title, 5);
            document.add(title);
    }
    private static void createTable(Document document) throws DocumentException {
        PdfPTable table = new PdfPTable(2);
        createCell("First name", 40, table);
        createCell("Kamil", 40, table);
        createCell("Last name", 40, table);
        createCell("Bieniasz", 40, table);
        createCell("Profession", 40, table);
        createCell("student", 40, table);
        createCell("Education", 60, table);
        createCell("Text", 60, table);
        createCell("Summary", 120, table);
        createCell("Text", 120, table);
        document.add(table);
    }
    private static void createCell(String text, int height,PdfPTable table){
        PdfPCell c1 = new PdfPCell(new Phrase(text));
        c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
        c1.setFixedHeight(height);
        table.addCell(c1);
    }
    private static void addEmptyLine(Paragraph paragraph, int line){
        for (int i = 0; i<line; i++){
            paragraph.add(new Paragraph(" "));
        }
    }
}
